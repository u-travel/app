package com.app.utravel.controllers;

import com.app.utravel.models.Attraction;
import com.app.utravel.models.Location;
import com.app.utravel.models.News;
import com.app.utravel.models.User;
import com.app.utravel.services.IAttractionService;
import com.app.utravel.services.ILocationService;
import com.app.utravel.services.INewsService;
import com.app.utravel.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class AttractionsController {

    @Autowired
    private IAttractionService attractionService;

    @Autowired
    private ILocationService locationService;

    @Autowired
    private INewsService newsService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping(path = "/attractions",produces = "application/html;charset=UTF-8")
    public String getAll(Model model) {
        List<Attraction> attractions = attractionService.findAll();
        model.addAttribute("attractions", attractions);
        return "attractions";
    }

    @RequestMapping(value="search", method=RequestMethod.GET)
    public String getSearched(@RequestParam(value="name", required=false) String name,
                              @RequestParam(value="type", required=false) String type,
                              Model model) {
        List<Attraction> attractions = attractionService.findByNameContaining(name);
        List<Attraction> types = attractions.stream()
                .filter(attraction -> type.equals(attraction.getType()))
                .collect(Collectors.toList());
        model.addAttribute("attractions", types);
        return "attractions";
    }

    @GetMapping("/attractions/{attractionId}")
    public String getById(@PathVariable Long attractionId,
                          Model model) {
        Optional<Attraction> attraction = attractionService.findById(attractionId);
        model.addAttribute("attraction", attraction.get());
        return "attractions_more";
    }

    @GetMapping("/attractions/addFavorite/{attractionId}")
    public String addToFavorite(@PathVariable Long attractionId,
                                Model model, Principal principal)
    {
        Optional<Attraction> attraction = attractionService.findById(attractionId);
        String name = principal.getName();
        Optional<User> user = userDetailsService.findByUserName(name);
        attraction.get().setUsersWhoLikeIt(new ArrayList<>(Arrays.asList(user.get())));
        attractionService.save(attraction.get());
        return "redirect:/attractions";
    }

    @GetMapping("/attractions/seed")
    public void seedDb() {
        Attraction att1 = new Attraction();
        Attraction att2 = new Attraction();
        Attraction att3 = new Attraction();
        Attraction att4 = new Attraction();

        att1.setName("Karkonosze");
        att2.setName("Beskidy");
        att3.setName("Pieniny");
        att4.setName("Tatry");

        att1.setType("Góry");
        att2.setType("Góry");
        att3.setType("Góry");
        att4.setType("Góry");

        att1.setImgSrc("/images/karkonosze_kategorie.jpg");
        att2.setImgSrc("/images/beskidy_kategorie.jpg");
        att3.setImgSrc("/images/pieniny_kategorie.jpg");
        att4.setImgSrc("/images/tatry_kategorie.jpg");

        Location loc1 = new Location();
        loc1.setCity("Karkonosze");
        loc1.setLongitude("15*41");
        loc1.setPostalCode("44-100");
        loc1.setStreet("Dworcowa");
        loc1.setStreetNo("467");
        loc1.setLatitude("65*13");
        List<Location> al=new ArrayList<>();
        locationService.save(loc1);

        List<Location>  ll = locationService.findByCityContaining("Karkonosze");
        att1.setLocations(ll);
        attractionService.save(att2);
        attractionService.save(att3);
        attractionService.save(att4);
        attractionService.save(att1);

        List<Location> loc = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            loc.add(new Location());
            loc.get(i).setCity("Warszawa" + i);
            loc.get(i).setPostalCode("44-10" + i);
            loc.get(i).setStreet("Akademicka" + i);
            loc.get(i).setStreetNo("12" + i);
            loc.get(i).setLongitude("54*1" + i);
            loc.get(i).setLatitude("14*3" + i);
            locationService.save(loc.get(i));
        }

        List<News> news = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            news.add(new News());
            news.get(i).setTitle("Aktualna pogoda" + i);
            news.get(i).setBody("Super słoneczko" + i);
            news.get(i).setType("info" + i);
            newsService.save(news.get(i));
        }

        Optional<Attraction> attrRel1 = attractionService.findByName("Karkonosze");
        List<News> nn = newsService.findByTitleContaining("Aktualna pogoda0");
        Optional<User> uu = userDetailsService.findByUserName("admin");
        attrRel1.get().setNews(nn);
        attrRel1.get().setUsersWhoLikeIt(new ArrayList<> (Arrays.asList(uu.get())));
        attractionService.save(attrRel1.get());
    }
}
