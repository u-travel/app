package com.app.utravel.controllers;

import com.app.utravel.models.User;
import com.app.utravel.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegistrationController {

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @GetMapping("/register")
    public String showRegistration(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register/save")
    public String saveUser(@ModelAttribute User user, Model model){
        model.addAttribute("user", user);
        if (user.getUserName().equals("admin")) {
            user.setRoles("ADMIN");
        }
        else {
            user.setRoles("USER");
        }
        user.setActive(true);
        userDetailsService.save(user);
        return "register";  //TODO Do a subpage with result of a registration
    }
}