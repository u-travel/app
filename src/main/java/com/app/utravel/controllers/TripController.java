package com.app.utravel.controllers;

import com.app.utravel.models.Attraction;
import com.app.utravel.models.User;
import com.app.utravel.services.IAttractionService;
import com.app.utravel.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.List;
import java.util.Optional;


@Controller
public class TripController {

    @Autowired
    private UserDetailsServiceImpl userServiceDetailsImpl;

    @Autowired
    private IAttractionService attractionService;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @GetMapping(path = "/trip_plan")
    public String getAll(Model model,
                         Principal principal) {

        String name = principal.getName();
        Optional<User> user= userServiceDetailsImpl.findByUserName(name);
        List<Attraction>attractions = user.get().getAttractions();
        model.addAttribute("attractions", attractions);
        return "trip_plan";
    }

    @GetMapping(path = "/trip_plan/delete/{attractionId}")
    public String deleteFavourite(@PathVariable Long attractionId,
                                  Model model,
                                  Principal principal) {
        String name = principal.getName();
        Optional<User> user = userDetailsService.findByUserName(name);
        Optional<Attraction> attraction = attractionService.findById(attractionId);
        List<User> userList = attraction.get().getUsersWhoLikeIt();
        userList.removeIf(u->u.getUserName().equals(name));
        attraction.get().setUsersWhoLikeIt(userList);
        attractionService.save(attraction.get());
        return "redirect:/trip_plan";
    }
}
