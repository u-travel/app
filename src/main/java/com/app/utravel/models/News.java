package com.app.utravel.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "news")
public class News {

    @Id
    @GeneratedValue
    private Long id;

    @NotBlank
    @Column(unique = true)
    private String title;

    @NotBlank
    private String body;

    @NotBlank
    private String type;

    @ManyToMany(mappedBy = "news", targetEntity = Attraction.class, fetch = FetchType.LAZY)
    private List<Attraction> attractions;
}
