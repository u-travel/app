package com.app.utravel.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "system_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    @Column(unique = true)
    private String userName;

    @NotBlank
    private String password;

    @NotNull
    private Boolean active;

    @NotBlank
    private String roles;

    @ManyToMany(mappedBy = "usersWhoLikeIt", targetEntity = Attraction.class, fetch = FetchType.LAZY)
    private List<Attraction> attractions;

    @OneToMany(targetEntity = Attraction.class, fetch = FetchType.EAGER)
    private List<Attraction> tripPlan;
}
