package com.app.utravel.models.converters;

import com.app.utravel.models.Attraction;
import com.app.utravel.services.IAttractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AttractionConverter implements Converter<Long, Attraction> {

    @Autowired
    IAttractionService attractionService;

    @Override
    public Attraction convert(Long id) {
        return attractionService.findById(id).get();
    }
}
