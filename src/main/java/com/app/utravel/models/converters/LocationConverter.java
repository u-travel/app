package com.app.utravel.models.converters;

import com.app.utravel.models.Location;
import com.app.utravel.services.ILocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class LocationConverter implements Converter<Long, Location> {

    @Autowired
    ILocationService locationService;

    @Override
    public Location convert(Long id) {
        return locationService.findById(id).get();
    }
}
