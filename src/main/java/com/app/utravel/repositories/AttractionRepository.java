package com.app.utravel.repositories;

import com.app.utravel.models.Attraction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AttractionRepository extends JpaRepository<Attraction, Long> {

    Optional<Attraction> findByName(String name);
    List<Attraction> findByNameContaining(String name);
    List<Attraction> findByType(String type);
    List<Attraction> findByLocationsId(Long id);
    List<Attraction> findByLocationsCityContaining(String city);
}
