package com.app.utravel.repositories;

import com.app.utravel.models.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {

    public List<News> findByTitleContaining(String title);
    public List<News> findByType(String type);
    public List<News> findByAttractionsId(Long id);
    public List<News> findByAttractionsNameContaining(String name);
    public List<News> findByAttractionsType(String type);
}
