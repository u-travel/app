package com.app.utravel.services;

import com.app.utravel.models.Attraction;
import com.app.utravel.repositories.AttractionRepository;
import com.app.utravel.repositories.LocationRepository;
import com.app.utravel.repositories.NewsRepository;
import com.app.utravel.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttractionService implements IAttractionService {

    @Autowired
    AttractionRepository attractionRepository;

    @Autowired
    LocationRepository locationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    NewsRepository newsRepository;

    @Override
    public List<Attraction> findAll() {
        return attractionRepository.findAll();
    }

    @Override
    public void save(Attraction attraction) {
        attractionRepository.save(attraction);
    }

    @Override
    public void deleteById(Long id) {
        attractionRepository.deleteById(id);
    }

    @Override
    public Optional<Attraction> findById(Long id) {
        return attractionRepository.findById(id);
    }

    @Override
    public Optional<Attraction> findByName(String name) {
        return attractionRepository.findByName(name);
    }

    @Override
    public List<Attraction> findByNameContaining(String name) {
        return attractionRepository.findByNameContaining(name);
    }

    @Override
    public List<Attraction> findByType(String type) {
        return attractionRepository.findByType(type);
    }

    @Override
    public List<Attraction> findByLocationId(Long id) {
        return attractionRepository.findByLocationsId(id);
    }

    @Override
    public List<Attraction> findByLocationCityContaining(String city) {
        return attractionRepository.findByLocationsCityContaining(city);
    }
}
