package com.app.utravel.services;

import com.app.utravel.models.Attraction;

import java.util.List;
import java.util.Optional;

public interface IAttractionService {

    List<Attraction> findAll();
    void save(Attraction attraction);
    void deleteById(Long id);
    Optional<Attraction> findById(Long id);
    Optional<Attraction> findByName(String name);
    List<Attraction> findByNameContaining(String name);
    List<Attraction> findByType(String type);
    List<Attraction> findByLocationId(Long id);
    List<Attraction> findByLocationCityContaining(String city);
}