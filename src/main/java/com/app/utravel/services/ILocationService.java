package com.app.utravel.services;

import com.app.utravel.models.Location;

import java.util.List;
import java.util.Optional;

public interface ILocationService {

    List<Location> findAll();
    void save(Location location);
    void deleteById(Long id);
    Optional<Location> findById(Long id);
    List<Location> findByCityContaining(String city);
    List<Location> findByStreetContaining(String street);
    List<Location> findByPostalCodeContaining(String street);
    List<Location> findByAttractionId(Long id);
    List<Location> findByAttractionNameContaining(String name);
}
