package com.app.utravel.services;

import com.app.utravel.models.User;

import java.util.List;
import java.util.Optional;

public interface IUserService {

    Optional<User> findById(Long id);
    Optional<User> findByUserName(String userName);
    List<User> findByUserNameContaining(String userName);
    List<User> findAll();
    void save(User user);
}
