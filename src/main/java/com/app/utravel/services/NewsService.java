package com.app.utravel.services;

import com.app.utravel.models.News;
import com.app.utravel.repositories.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NewsService implements INewsService {

    @Autowired
    NewsRepository newsRepository;

    @Override
    public List<News> findAll() {
        return newsRepository.findAll();
    }

    @Override
    public void save(News news) {
        newsRepository.save(news);
    }

    @Override
    public void deleteById(Long id) {
        newsRepository.deleteById(id);
    }

    @Override
    public Optional<News> findById(Long id) {
        return newsRepository.findById(id);
    }

    @Override
    public List<News> findByTitleContaining(String title) {
        return newsRepository.findByTitleContaining(title);
    }

    @Override
    public List<News> findByType(String type) {
        return newsRepository.findByType(type);
    }

    @Override
    public List<News> findByAttractionId(Long id) {
        return newsRepository.findByAttractionsId(id);
    }

    @Override
    public List<News> findByAttractionsNameContaining(String name) {
        return newsRepository.findByAttractionsNameContaining(name);
    }

    @Override
    public List<News> findByAttractionsType(String type) {
        return newsRepository.findByAttractionsType(type);
    }
}
